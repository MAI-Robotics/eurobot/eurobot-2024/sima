#include "sensors.h"

VL53L0X sensorleft;
VL53L0X sensorcenter;
VL53L0X sensorright;

bool sensorleftActive;
bool sensorcenterActive;
bool sensorrightActive;

void startSensors() {
    Serial.println("Sensor Start");
    Wire.begin(D2, D1); // SDA,SCL

    pinMode(XSHUT_LEFT, OUTPUT);
    pinMode(XSHUT_CENTER, OUTPUT);
    pinMode(XSHUT_RIGHT, OUTPUT);

    //Turn off sensors for initialisation
    digitalWrite(XSHUT_LEFT, LOW);
    digitalWrite(XSHUT_CENTER, LOW);
    digitalWrite(XSHUT_RIGHT, LOW);
    delay(10);

    Serial.println("Xshut pins set");
    //Sensor left
    digitalWrite(XSHUT_LEFT, HIGH);
    delay(10); // debug   
    if (!sensorleft.init()) {
        Serial.println("Failed to initialize Sensor left");
        sensorleftActive = false;
    }
    else{
        sensorleftActive = true;
    }
    sensorleft.setAddress(0x30); // I2C-Adress of left

    //Sensor center
    digitalWrite(XSHUT_CENTER, HIGH);
    delay(10); // debug
    if (!sensorcenter.init()) {
        Serial.println("Failed to initialize Sensor Center");
        sensorcenterActive = false;
    }
    else{
        sensorcenterActive = true;
    }
    sensorcenter.setAddress(0x31); // I2C-Adress of center

    digitalWrite(XSHUT_RIGHT, HIGH);
    delay(10); // debug
    if (!sensorright.init()) {
        Serial.println("Failed to initialize Sensor right");
        sensorrightActive = false;
    }
    else{
        sensorrightActive = true;
    }
    sensorcenter.setAddress(0x32); // I2C-Adress of center

    //start
    sensorleft.startContinuous();
    sensorcenter.startContinuous();
    sensorright.startContinuous();

    //debug
    if (sensorleftActive == true && sensorcenterActive == true && sensorrightActive ==true) {
        Serial.println("sensors ready");
    }
}

uint16_t getDistanceLeft() {
    uint16_t distance = sensorleft.readRangeContinuousMillimeters();
    return distance;
}

uint16_t getDistanceCenter() {
    uint16_t distance = sensorcenter.readRangeContinuousMillimeters();
    return distance;
}

uint16_t getDistanceRight() {
    uint16_t distance = sensorright.readRangeContinuousMillimeters();
    return distance;
}
