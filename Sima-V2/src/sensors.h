#ifndef SENSORS_H
#define SENSORS_H
#include "libs.h"

VL53L0X sensorleft;
VL53L0X sensorcenter;
VL53L0X sensorright ;

bool sensorleftActive;
bool sensorcenterActive;
bool sensorrightActive;

enum SensorID {
    LEFT_SENSOR = 1,
    RIGHT_SENSOR = 2,
    CENTER_SENSOR = 3
};

void startSensors();

uint16_t getDistanceLeft() ;

uint16_t getDistanceCenter();
uint16_t getDistanceRight();

#endif