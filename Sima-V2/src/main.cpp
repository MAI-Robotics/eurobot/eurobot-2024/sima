#include "main.h"

unsigned long startTime = 0;
bool timeOver = false;

void setup() {
  Serial.begin(115200);
  Serial.println("started");
  startSensors();
}


void waitForPullcord() {
  while (digitalRead(Pullcord) == HIGH){
    delay(5);
    yield();
  }
  startTime = millis();
}

void checkTime() {
  if (millis() - startTime > 15000){ //the last 15sec of the match
    digitalWrite(EN_PIN, HIGH);
    // ACTUATOR
    while (true){
      yield();
      delay(100);
    }
  }
}

void loop() {
  yield();
  Serial.print("Left: ");
  Serial.println(getDistanceLeft());
  delay(10);
  Serial.print("Center:");
  Serial.println(getDistanceCenter());
  delay(10);
  Serial.print("Right:");
  Serial.println(getDistanceRight());
  delay(500);
}
