#include <Arduino.h>
#include <Servo.h>

#define SERVO_LEFT_PIN D5
#define SERVO_RIGHT_PIN D7

#define LED D0

Servo servoLeft;
Servo servoRight;

int speed;

bool run = false;

//For letters
char* letters[] = {
".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", // A-I
".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", // J-R 
"...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.." // S-Z
};

int dotDelay = 200;

void flashDotOrDash(char dotOrDash) {
  digitalWrite(LED, LOW); 
  if (dotOrDash == '.')
  {
      delay(dotDelay);
  }
  else // must be a - 
  {
    delay(dotDelay * 3); 
  }
  digitalWrite(LED, HIGH);
  delay(dotDelay);
}

void IRAM_ATTR emergencyRun(){
  run = true;
}

void runAway(){
  if(run){
    servoLeft.write(180);
    servoRight.write(0);

    delay(2000);

    run = false;
  }
}

void setup(){
  pinMode(D6, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(D6), emergencyRun, FALLING);

  pinMode(D0, OUTPUT);

  servoLeft.attach(SERVO_LEFT_PIN);
  servoRight.attach(SERVO_RIGHT_PIN);

  digitalWrite(LED, HIGH);
  
  while(!run){
    delay(100);
  }

}

void loop(){
  servoLeft.write(run?0:random(0, 180));
  servoRight.write(run?180:random(0, 180));
  runAway();
  flashDotOrDash('-');
  runAway();
  flashDotOrDash('-');
  runAway();

  delay(dotDelay * 3);

  servoLeft.write(run?0:random(0, 180));
  servoRight.write(run?180:random(0, 180));
  runAway();
  flashDotOrDash('.');
  runAway();
  flashDotOrDash('-');
  runAway();

  delay(dotDelay * 3);

  servoLeft.write(run?0:random(0, 180));
  servoRight.write(run?180:random(0, 180));
  runAway();
  flashDotOrDash('.');
  runAway();
  flashDotOrDash('.');
  runAway();

  delay(dotDelay * 5);
}