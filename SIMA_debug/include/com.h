#include <ESP8266WiFi.h>
#include <espnow.h>
#include <Servo.h>

// REPLACE WITH THE MAC Address of your receiver 
uint8_t broadcastAddress[] = {0x4c, 0x75, 0x25, 0x37, 0x19, 0xF4};

// Variable to store if sending data was successful
String success;

//Must match the receiver structure
typedef struct struct_message {
  char colour[32];
  int success;
  int sima_ID;
  int path;
} struct_message;

bool flag = false;

// Create a struct_message called myData
struct_message myData;

unsigned long lastTime = 0;  
unsigned long timerDelay = 2000;  // send readings timer

Servo servoLeft;
Servo servoRight;

int startMillis;

void checkButton(){
  int buttonState = digitalRead(D6);

  while(buttonState){
    buttonState = digitalRead(D6);
    delay(10);
  }
}

// Callback when data is sent
void OnDataSent(uint8_t *mac_addr, uint8_t sendStatus) {
  Serial.print("Last Packet Send Status: ");
  if (sendStatus == 0){
    Serial.println("Delivery success");
  }
  else{
    Serial.println("Delivery fail");
  }
}

// Callback function that will be executed when data is received
void OnDataRecv(uint8_t * mac, uint8_t *incomingData, uint8_t len) {
  memcpy(&myData, incomingData, sizeof(myData));
  Serial.print("Bytes received: ");
  Serial.println(len);
  Serial.print("colour: ");
  Serial.println(myData.colour);
  Serial.print("Success: ");
  Serial.println(myData.success);
  Serial.print("sima ID: ");
  Serial.println(myData.sima_ID);
  
  flag = true;
}