#include <Wire.h>
#include <VL53L0X.h>

// Sensorobjekte
VL53L0X sensor1;
VL53L0X sensor2;

// XSHUT-Pins für die Sensoren
#define XSHUT_SENSOR1 D0 // GPIO16
#define XSHUT_SENSOR2 D3 // GPIO0

void setup() {
  Serial.begin(9600);
  Wire.begin(D2, D1); // SDA und SCL

  // XSHUT-Pins als Ausgang konfigurieren
  pinMode(XSHUT_SENSOR1, OUTPUT);
  pinMode(XSHUT_SENSOR2, OUTPUT);

  // Beide Sensoren ausschalten
  digitalWrite(XSHUT_SENSOR1, LOW);
  digitalWrite(XSHUT_SENSOR2, LOW);
  delay(10);

  // Sensor 1 aktivieren und initialisieren
  digitalWrite(XSHUT_SENSOR1, HIGH);
  delay(10); // Warten, bis der Sensor bereit ist
  if (!sensor1.init()) {
    Serial.println("Failed to initialize Sensor 1!");
    while (1);
  }
  sensor1.setAddress(0x30); // I2C-Adresse des Sensors 1 ändern

  // Sensor 2 aktivieren und initialisieren
  digitalWrite(XSHUT_SENSOR2, HIGH);
  delay(10); // Warten, bis der Sensor bereit ist
  if (!sensor2.init()) {
    Serial.println("Failed to initialize Sensor 2!");
    while (1);
  }
  sensor2.setAddress(0x31); // I2C-Adresse des Sensors 2 ändern

  // Beide Sensoren auf kontinuierliche Messung setzen
  sensor1.startContinuous();
  sensor2.startContinuous();

  Serial.println("Beide Sensoren initialisiert und bereit.");
}

void loop() {
  // Messwerte von Sensor 1 lesen
  uint16_t distance1 = sensor1.readRangeContinuousMillimeters();
  if (sensor1.timeoutOccurred()) {
    Serial.println("Timeout occurred on Sensor 1!");
  } else {
    Serial.print("Sensor 1 Distance: ");
    Serial.print(distance1);
    Serial.println(" mm");
  }

  // Messwerte von Sensor 2 lesen
  uint16_t distance2 = sensor2.readRangeContinuousMillimeters();
  if (sensor2.timeoutOccurred()) {
    Serial.println("Timeout occurred on Sensor 2!");
  } else {
    Serial.print("Sensor 2 Distance: ");
    Serial.print(distance2);
    Serial.println(" mm");
  }

  delay(500); // 500 ms Verzögerung zwischen Messungen
}
